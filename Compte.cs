using client.Mada;
public class Compte
{
    public float Solde { get; private set; }
    public uint NumCompte { get; private set; }
    public Client Proprietaire { get; set; }
    private static uint compteur = 0; 

    public Compte()
    {
        Solde = default; 
        NumCompte = ++compteur; 
        Proprietaire = new(); 
    }

    public Compte(Client proprietaire) :
        this() 
    {
        Proprietaire = proprietaire ?? new(); 
    }

    public Compte(float solde, Client proprietaire) :
        this(proprietaire) 
    {
        Solde = solde; 
    }

    public void Crediter(float montant)
    {
        Solde += montant; 
    }

    public void Crediter(float montant, Compte crediteur)
    {
        Crediter(montant); 
        crediteur.Debiter(montant); 
    }

    public void Debiter(float montant)
    {
        Solde -= montant; 
    }


    public void Debiter(float montant, Compte debiteur)
    {
        Debiter(montant); 
        debiteur.Crediter(montant); 
    }

    public string Afficher()
    {
        return $@"
        _____________________________
        Numéro de Compte: {NumCompte}
        Solde de compte: {Solde}
        Propriétaire du compte :
        {Proprietaire.Afficher()}
        ____________________________";
    }

    public static string NbCompte()
    {
        return $"Le nombre de comptes créés: {compteur}";
    }
    
}
