namespace client.Mada
{
    public class Client
    {
        public string Nom { get; private set; }
        public string Prenom { get; private set; }
        public string CIN { get; private set; }
        public string Tel { get; private set; }

        public Client()
        {
            Nom = "";
            Prenom = "";
            CIN = "";
            Tel = "";
        }

        public Client(string cin) :
            this() 
        {
            CIN = cin;
        }
        public Client(string cin, string nom) :
            this(cin) 
        {
            
            Nom = nom;
        }

        public Client(string cin, string nom, string prenom) :
            this(cin, nom) 
        {
            Prenom = prenom;
        }
        public Client(string cin, string nom, string prenom, string tel) :
            this(cin, nom, prenom) 
        {
            Tel = tel;
        }

        public string Afficher()
        {
            return $@"CIN: {CIN}
                      Nom: {Nom}
                      Prénom: {Prenom}
                      Tél: {Tel}";
        }
    }
}