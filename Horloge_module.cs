using System;
using System.Collections.Generic;
using System.Linq;
namespace Horloge;

class Alarme
{
    public string Nom { get; set; }
    public string Heure { get; set; }
    public string Date { get; set; }
    public bool LancerUneFois { get; set; }
    public bool Periodique { get; set; }
    public bool SonnerieParDefaut { get; set; }
}

class Horloge
{
    public string Ville { get; set; }
    public string DifferenceGMT { get; set; }
}

class HorlogeApp
{
    private List<Alarme> alarmes;
    private List<Horloge> horloges;

    public HorlogeApp()
    {
        alarmes = new List<Alarme>();
        horloges = new List<Horloge>();
    }

    public void Executer()
    {
        while (true)
        {
            AfficherMenuPrincipal();
            var choix = Console.ReadLine();
            TraiterChoixPrincipal(choix);
        }
    }

    private void AfficherMenuPrincipal()
    {
        Console.WriteLine("\nBienvenue dans le menu principal");
        Console.WriteLine("Veuillez chosir une option :");
        Console.WriteLine("1 - Alarme");
        Console.WriteLine("2 - Horloge");
        Console.WriteLine("5 - Quitter");
        Console.Write("Quel est votre choix ? ");
    }

    private void TraiterChoixPrincipal(string choix)
    {
        switch (choix)
        {
            case "1":
                MenuAlarme();
                break;
            case "2":
                MenuHorloge();
                break;
            case "5":
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Choix invalide. Veuillez entrer 1, 2 ou 5.");
                break;
        }
    }

    private void MenuAlarme()
    {
        while (true)
        {
            Console.WriteLine("\nQuel est votre choix ?");
            Console.WriteLine("1 - Voir les Alarmes actives");
            Console.WriteLine("2 - Créer une Alarme");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
            var choix = Console.ReadLine();

            switch (choix)
            {
                case "1":
                    VoirAlarmesActives();
                    break;
                case "2":
                    CreerAlarme();
                    break;
                case "3":
                    return;
                default:
                    Console.WriteLine("Choix invalide. Veuillez entrer 1 , 2 ou 3.");
                    break;
            }
        }
    }

    private void VoirAlarmesActives()
    {
        Console.WriteLine("\nListe d'alarmes actives:");
        foreach (var alarme in alarmes)
        {
            Console.WriteLine($"{alarme.Nom} - {alarme.Heure}");
            if (alarme.Periodique)
            {
                Console.WriteLine($"Date alarme {alarme.Date}");
            }
            else
            {
                Console.WriteLine($"{alarme.Date}");
            }
        }
    }

    private void CreerAlarme()
    {
        var nouvelleAlarme = new Alarme();

        Console.Write("Donnez un nom de l'alarme : ");
        nouvelleAlarme.Nom = Console.ReadLine();

        Console.Write("Heure de l'alarme ( h:mn) : ");
        nouvelleAlarme.Heure = Console.ReadLine();

        Console.Write("Date de planification (L/M/Me/J/V/S/D) : ");
        nouvelleAlarme.Date = Console.ReadLine();

        Console.Write("Lancer une seule fois (o/n) : ");
        nouvelleAlarme.LancerUneFois = Console.ReadLine().ToLower() == "o";

        Console.Write("Periodique (o/n) : ");
        nouvelleAlarme.Periodique = Console.ReadLine().ToLower() == "o";

        Console.Write("Activer sonnerie par défaut (o/n) : ");
        nouvelleAlarme.SonnerieParDefaut = Console.ReadLine().ToLower() == "o";

        alarmes.Add(nouvelleAlarme);

        Console.WriteLine("Alarme enregistrer :");
    }

    private void MenuHorloge()
    {
        while (true)
        {
            Console.WriteLine("\nQuel est votre choix ?");
            Console.WriteLine("1 - Voir les Horloges actives");
            Console.WriteLine("2 - Ajouter une Horloge");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");
            var choix = Console.ReadLine();

            switch (choix)
            {
                case "1":
                    VoirHorlogesActives();
                    break;
                case "2":
                    AjouterHorloge();
                    break;
                case "3":
                    return;
                default:
                    Console.WriteLine("Choix invalide. Veuillez entrer 1 , 2 ou 3.");
                    break;
            }
        }
    }

    private void VoirHorlogesActives()
    {
        Console.WriteLine("\nListe d'horloges actives:");
        foreach (var horloge in horloges)
        {
            Console.WriteLine($"{horloge.Ville} - {CalculerHeureLocale(horloge.DifferenceGMT)}");
        }
    }

    private void AjouterHorloge()
    {
        Console.Write("Choisissez une ville: ");
        var ville = Console.ReadLine();

        Console.Write("Horloge enregistree :");
        horloges.Add(new Horloge { Ville = ville });
    }

    private string CalculerHeureLocale(string differenceGMT)
    {
        var heureGMT = DateTime.UtcNow.Add(TimeSpan.Parse(differenceGMT));
        return heureGMT.ToString("h:mn:sc");
    }
}
    

