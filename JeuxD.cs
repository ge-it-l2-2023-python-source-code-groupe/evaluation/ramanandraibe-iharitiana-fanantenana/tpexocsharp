using System;
using  TpExoCsharp;

using System.Collections.Generic;
using System.Linq;
namespace Jeu_de_des;

class Joueur
{
    public string Nom { get; set; }
    public int Score { get; set; }
}

class Manche
{
    private List<Joueur> joueurs;
    private Dictionary<string, int> resultats;

    public Manche(List<Joueur> joueurs)
    {
        this.joueurs = joueurs;
        resultats = new Dictionary<string, int>();
    }

    public void Jouer()
    {
        Console.WriteLine($"Manche en cours...");

        foreach (var joueur in joueurs)
        {
            Console.Write($"Tour de {joueur.Nom} - Voulez-vous lancer le dé ? (o or n) ");
            var input = Console.ReadLine();

            if (input.ToLower() == "o")
            {
                var resultat = new Random().Next(1, 7);
                resultats[joueur.Nom] = resultat;
                AfficherResultat(joueur, resultat);
            }
            else
            {
                Console.WriteLine($"{joueur.Nom} passe son tour.");
            }
        }

        var meilleursJoueurs = ObtenirMeilleursJoueurs();
        ActualiserScores(meilleursJoueurs);
    }

    private void AfficherResultat(Joueur joueur, int resultat)
    {
        Console.WriteLine($"{joueur.Nom} a obtenu un tirage de {resultat}.");
    }

    private List<string> ObtenirMeilleursJoueurs()
    {
        var meilleurTirage = resultats.Values.Max();
        var meilleursJoueurs = new List<string>();

        foreach (var (nom, tirage) in resultats)
        {
            if (tirage == meilleurTirage)
            {
                meilleursJoueurs.Add(nom);
            }
        }

        return meilleursJoueurs;
    }

    private void ActualiserScores(List<string> meilleursJoueurs)
    {
        foreach (var joueur in joueurs)
        {
            if (meilleursJoueurs.Contains(joueur.Nom))
            {
                if (resultats[joueur.Nom] == 6)
                {
                    joueur.Score += 2;
                }
                else
                {
                    joueur.Score += 1;
                }
            }
        }
    }

    public Dictionary<string, int> Resultats => resultats;
}

class Application
{
    private List<Joueur> joueurs;
    private int mancheNumero;
    private List<Tuple<DateTime, Dictionary<string, int>>> mancheHistorique;

    public Application()
    {
        joueurs = new List<Joueur>();
        mancheNumero = 1;
        mancheHistorique = new List<Tuple<DateTime, Dictionary<string, int>>>();
    }

    public void Executer()
    {
        while (true)
        {
            AfficherMenuPrincipal();
            var choix = Console.ReadLine();
            TraiterChoix(choix);
        }
    }

    private void AfficherMenuPrincipal()
    {
        AfficherScores();
        Console.WriteLine("Veuillez choisir une option:");
        Console.WriteLine("1 - Nouvelle manche");
        Console.WriteLine("2 - Voir l'historique des manches");
        Console.WriteLine("3 - Quitter");
    }

    private void AfficherScores()
    {
        foreach (var joueur in joueurs.OrderByDescending(j => j.Score))
        {
            Console.WriteLine($"- {joueur.Nom} : {joueur.Score} points");
        }
    }

    private void TraiterChoix(string choix)
    {
        switch (choix)
        {
            case "1":
                NouvelleManche();
                break;
            case "2":
                AfficherHistoriqueManches();
                break;
            case "3":
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Choix invalide. Veuillez entrer 1 , 2 ou 3");
                break;
        }
    }

    private void NouvelleManche()
    {
        Console.WriteLine($"\nManche {mancheNumero}");
        var joueurs = DemanderJoueurs();
        var manche = new Manche(joueurs);
        manche.Jouer();
        mancheHistorique.Add(new Tuple<DateTime, Dictionary<string, int>>(DateTime.Now, manche.Resultats));
        mancheNumero++;
    }

    private List<Joueur> DemanderJoueurs()
    {
        Console.Write("Entrez le nombre de joueurs : ");
        var nombreJoueurs = int.Parse(Console.ReadLine());
        var joueurs = new List<Joueur>();

        for (int i = 0; i < nombreJoueurs; i++)
        {
            Console.Write("Veuillez saisir le nom du joueur : ");
            var nomJoueur = Console.ReadLine();
            joueurs.Add(new Joueur { Nom = nomJoueur });
        }

        return joueurs;
    }

    private void AfficherHistoriqueManches()
    {
        Console.WriteLine("\nHistorique des manches:");

        for (int i = 0; i < mancheHistorique.Count; i++)
        {
            var (date, resultats) = mancheHistorique[i];
            Console.WriteLine($"\nManche {i + 1} - Date et heure : {date}");

            foreach (var (joueur, tirage) in resultats)
            {
                Console.WriteLine($"{joueur} a obtenu un tirage de {tirage}.");
            }
        }

        Console.WriteLine("\nAppuyez sur une touche pour revenir au menu principal.");
        Console.ReadKey();
    }
}


