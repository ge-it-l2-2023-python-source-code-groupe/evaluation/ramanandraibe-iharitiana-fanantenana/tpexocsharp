﻿using System;
using System.ComponentModel.Design;
using System.Collections.Generic;
namespace TpExoCsharp;
using System.Security.Cryptography.X509Certificates;
using client.Mada;
using Horloge;
using Jeu_de_des;

class Program
{
    static List<Compte> listeDesComptes = new List<Compte>();
    static List<string> ListeJoueurs = new List<string>();
    static int nombreDeComptesCrees = 0;
      
    static void Main(string[] args)
    {
        
        while (true)
        {
            Menu();

        Console.Write("Veuillez saisir votre choix : ");
        string choix = Console.ReadLine();

        if(choix == "1")
        {
            Banque();
        }
        else if (choix == "2")
        {
            Cercle();
        }
        else if (choix == "3")
        {
            Jeux();
        }
        else if(choix == "4"){
        {
            Horloge();
        }
        }
        else if (choix == "0")
        {
            continue;
        }
        else if (choix == "5"){
            break;
        }
        else 
        {
            Console.WriteLine ("Votre choix n'est pas valide");

        }
        

    }
}
    static void JeuxD()
    {
        Console.WriteLine("Veuillez saisir le nombre de joueur :");
        int ? nbrJoueur = int.Parse(Console.ReadLine());
        for(int i = 1;i<= nbrJoueur;i++)
        {
            Console.WriteLine($"Enter le nom du joueur {i} :");
            String Joueur = Console.ReadLine();
            ListeJoueurs.Add(Joueur);

        }
        
    }
    
    static void Cercle(){

        Console.Write("Donner l'abscice du centre : ");
        double x=double.Parse(Console.ReadLine());

        Console.Write("Donner l'ordonner du centre : ");
        double y=double.Parse(Console.ReadLine());

        Console.Write("Donner le rayon: ");
        double r=double.Parse(Console.ReadLine());

        Cercle point = new Cercle((double)x,(double)y,(double)r);
        point.Display();
        point.Perimetre();
        point.Surface();

        Console.WriteLine("Donner un point : ");
        Console.Write("x: ");
        double monx =double.Parse(Console.ReadLine());
        Console.Write("y:");
        double mony = double.Parse(Console.ReadLine());
        
    }
     static void Horloge()
    {
        var app = new HorlogeApp();
        app.Executer();
    }

    static void Menu(){
        Console.WriteLine("Menu principal:");
        Console.WriteLine("_______________");
        Console.WriteLine("1.Compte en banque");
        Console.WriteLine("2.Cercle");
        Console.WriteLine("3.Jeux de des");
        Console.WriteLine("4.Horloge module");
        Console.WriteLine("5.Quitter");
    }
 
    static void Banque (){
            Console.WriteLine("_____________________________");
            Console.WriteLine("Entrer 1 pour Commencer");
            string choix2 = Console.ReadLine();
            Console.Write("__________________________\n");
            
            
        if(choix2 == "1")
        { 
            Compte1();
        }
        else if(choix2 == "0")
        {
            Menu();
        }
        static void Compte1 ()
        {
            if(nombreDeComptesCrees == 0)
            {
            String cin, lastN, firstN, tel;

            Console.Write($"Compte N*1 :\n");
            Console.Write($"Donner Le CIN: ");
            cin = Console.ReadLine() ?? "";
            Console.Write($"Veuillez saisir votre Nom: ");
            lastN = Console.ReadLine() ?? "";
            Console.Write($"Veuillez saisir votre Prenom: ");
            firstN = Console.ReadLine() ?? "";
            Console.Write($"Telephone: ");
            tel= Console.ReadLine() ?? "";
            Console.Write("____________________\n");
            

            Compte cpt= new Compte(new Client(cin, firstN, lastN, tel));
            listeDesComptes.Add(cpt);
            nombreDeComptesCrees++;
            Console.WriteLine($"Détails du compte:{cpt.Afficher()}");
            Console.WriteLine (@"1.Creer un autre compte
2.Autre fonctionnalites
0.Menu principal ");
            Console.Write("Quel est votre choix? : ");
            string choix3d = Console.ReadLine();
            if(choix3d == "1")
            {
                Compte();
            }
            else if(choix3d == "2")
            {
                choix();
            }
            else if(choix3d == "0")
            {
                Menu();
            }
            else
            {
                Console.WriteLine ("Votre choix n'est pas valide");
            }
            

            Console.WriteLine("_____________________");
            Console.WriteLine($"Nombre total de compte crees : {nombreDeComptesCrees}");

            }
            else
            {
                Compte();
            }
        }
        static void Compte() {
        Console.WriteLine("Veuillez saisir le nombre de compte a creer :");
        Console.WriteLine("0 pour sauter l'etape");
        int ? nbrCompte = int.Parse(Console.ReadLine());
        String cin, lastN, firstN, tel;
        for(int i=nombreDeComptesCrees;i<=nbrCompte;i++)
            { 
                        
            Console.Write($"Compte {nombreDeComptesCrees + 1} :");
            Console.Write("Donner Le CIN: ");
            cin = Console.ReadLine() ?? "";
            Console.Write($"Veuillez saisir votre Nom: ");
            lastN = Console.ReadLine() ?? "";
            Console.Write($"Veuillez saisir votre Prenom: ");
            firstN = Console.ReadLine() ?? "";
            Console.Write($"Telephone: ");
            tel= Console.ReadLine() ?? "";
            Console.Write("____________________\n");
            Compte cpt= new Compte(new Client(cin, firstN, lastN, tel));
            listeDesComptes.Add(cpt);
            nombreDeComptesCrees++;
            choix();
        }}
        static void choix() {
            Console.WriteLine("1.Afficher les comptes existant");
            Console.WriteLine("2.Deposer un montant");
            Console.WriteLine("3.Retirer un montant");
            Console.WriteLine("4.Creer un autre compte");
            Console.WriteLine("0.Menu Principal");
            Console.WriteLine("______________________");
            Console.Write("Quel est votre choix? : ");
            string choixMontant = Console.ReadLine();
                if(choixMontant == "1")
                {
                    AfficheComptes();
                }
                else if(choixMontant == "2")
                {
                    EffectuerDepot();
                }
                else if(choixMontant == "3")
                {
                    EffectuerRetrait();
                }
                else if(choixMontant == "0")
                {
                    Menu();
                }
                else if(choixMontant == "4")
                {
                    Compte();
                }
                else
                {
                Console.WriteLine ("Votre choix n'est pas valide");
                }
            }

            static void AfficheComptes()
                {
                Console.WriteLine("Liste des comptes :");
                foreach (Compte cpt in listeDesComptes)
                 {
                    Console.WriteLine($@"Details du compte : 
{cpt.Afficher()}");
                    Console.WriteLine("______________________");
                 }
                 Compte();
                }
           static void EffectuerDepot()
                {
                Console.Write("Enter le numero de la compte a debiter: ");
                int numeroCompte = int.Parse(Console.ReadLine());
                Console.Write("Donner le montant a deposer: ");
                float montant = float.Parse(Console.ReadLine());

                Compte compte = listeDesComptes.Find(c => c.NumCompte == numeroCompte);
                if (compte != null)
                    {
                    compte.Crediter(montant);
                    Console.WriteLine($"Opération de depot bien effectuee. Nouveau solde = {compte.Solde}");
                    Console.WriteLine("________________________");
                    }
                else
                    {
                    Console.WriteLine("Numero de compte invalide.");
                    Console.WriteLine("_______________________");
                    }
                 Compte();                    
                }
            static void EffectuerRetrait()
            {
            Console.Write("Enter le numero de la compte a debiter: ");
            int numeroCompte = int.Parse(Console.ReadLine());
            Console.Write("Donner le montant à retirer: ");
            float montant = float.Parse(Console.ReadLine());
            Compte compte = listeDesComptes.Find(c => c.NumCompte == numeroCompte);
            if (compte != null)
                {
                if (compte.Solde >= montant)
                    {
                    compte.Debiter(montant);
                    Console.WriteLine($"Operation de retrait bien effectuee. Nouveau solde = {compte.Solde}");
                    Console.WriteLine("________________________");
                    }
                else
                {
                    Console.WriteLine("Votre solde est insuffisant.");
                    Console.WriteLine("________________________");
                }
                }
                else
                {
                    Console.WriteLine("Numero de compte invalide.");
                    Console.WriteLine("_________________________");
                }
                 Compte();
            }
     }
    static void Manche()
    {
        DateTime Date = DateTime.Now;
        string DateStyle = "dd/MM/yyyy HH:mm:ss";
        string DateJeux = Date.ToString(DateStyle);
    }
    static void Jeux()
    {
        var app = new Application();
        app.Executer();
    }
    
    
}
